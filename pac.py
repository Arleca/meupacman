# meu jogo do pacman
import pygame
LARGURA_TELA = 800
ALTURA_TELA = 600
TAMANHO_BASE = 40
LARGURA_JOGO = LARGURA_TELA // TAMANHO_BASE
ALTURA_JOGO = ALTURA_TELA // TAMANHO_BASE
matriz_jogo = []
pac_id = 1
pac_x = 1
pac_y = 1
fan_1_id = 2
fan_1_y = 3
fan_1_x = 3

#criando matriz 
for i in range(ALTURA_JOGO):
    matriz_jogo.append([])
    for j in range(LARGURA_JOGO):
        matriz_jogo[i].append(0)

for i in range(ALTURA_JOGO):
    for j in range(LARGURA_JOGO):
        if j==0 or i==0 or j==LARGURA_JOGO-1 or i==ALTURA_JOGO-1:
            matriz_jogo[i][j]=-1

#carregando imagem
pac_image = pygame.image.load('pac.png')
fan_1_image = pygame.image.load('ghost1.png')
fan_1_x=5
fan_1_y=5
#imprimir matriz do jogo para saber onde estão informações
def imprimir_matriz_jogo():
    print("-"*30)
    for i in range(ALTURA_JOGO):
        for j in range(LARGURA_JOGO):
            print(matriz_jogo[i][j], end='')
        print()
    print("-"*30)
imprimir_matriz_jogo()
matriz_jogo[pac_y][pac_x] = pac_id
matriz_jogo[fan_1_y][fan_1_x] = fan_1_id
imprimir_matriz_jogo()

#inicializando pygame
pygame.init()
#criando tela principal
screen = pygame.display.set_mode((LARGURA_TELA, ALTURA_TELA))
#loop principal
done = False
clock = pygame.time.Clock()
while not done:
    #limpar a tela
    screen.fill((0, 0, 0)) 
    #tratar eventos do jogo
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    #capturar teclado
    pressed = pygame.key.get_pressed()

    # verificando movimento para esquerda
    if pressed[pygame.K_LEFT]: 
        if matriz_jogo [pac_y][pac_x-1] == 0:
            matriz_jogo[pac_y][pac_x] = 0
            pac_x -= 1
            matriz_jogo[pac_y][pac_x] = pac_id
            imprimir_matriz_jogo()

    # verificando movimento para direita
    if pressed[pygame.K_RIGHT]: 
        if matriz_jogo [pac_y][pac_x+1] == 0:
            matriz_jogo[pac_y][pac_x] = 0
            pac_x += 1
            matriz_jogo[pac_y][pac_x] = pac_id
            imprimir_matriz_jogo()

    # verificando movimento para UP
    if pressed[pygame.K_UP]: 
        if matriz_jogo [pac_y-1][pac_x] == 0:
            matriz_jogo[pac_y][pac_x] = 0
            pac_y -= 1
            matriz_jogo[pac_y][pac_x] = pac_id
            imprimir_matriz_jogo()

    # verificando movimento para direita
    if pressed[pygame.K_DOWN]: 
        if matriz_jogo [pac_y+1][pac_x] == 0:
            matriz_jogo[pac_y][pac_x] = 0
            pac_y += 1
            matriz_jogo[pac_y][pac_x] = pac_id
            imprimir_matriz_jogo()


    #atualizar tela
    screen.blit(pac_image,(pac_x*TAMANHO_BASE,pac_y*TAMANHO_BASE))
    screen.blit(fan_1_image,(fan_1_x*TAMANHO_BASE,fan_1_y*TAMANHO_BASE))
    pygame.display.flip()
    clock.tick(60)
